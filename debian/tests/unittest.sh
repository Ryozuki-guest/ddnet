#!/bin/sh
set -e

mkdir build
cd build
cmake ..
make run_tests
cd ..
rm -rf build
